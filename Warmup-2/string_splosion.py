# https://codingbat.com/prob/p118366


def string_splosion(str):
    temp = ''
    i = 0
    while i < len(str) + 1:
        temp = temp + str[:i]
        i += 1
    return temp

#temp = ""
#result = ""
#for i in range(len(str)):
#    temp += str[i]
#    result += temp
#return result

#    temp = [str[:i + 1] for i in range(len(str))]
#    return "".join(temp)

#    return "".join([str[:i + 1] for i in range(len(str))])

print(string_splosion('Code'))
print(string_splosion('abc'))
print(string_splosion('ab'))