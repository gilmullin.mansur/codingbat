# https://codingbat.com/prob/p145834


def last2(str):
    if len(str) <= 2:
        return 0
  
    last2 = str[-2:]
    count = 0

    for i in range(len(str) - 2):
        sub = str[i: i + 2]
        if sub == last2:
            count += 1

    return count

# return str.count(str[-2:]) - 1


#  return len([i for i in range(len(str) - 2) if str[i:i+2] == str[-2:]])
  