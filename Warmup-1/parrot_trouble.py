# https://codingbat.com/prob/p166884
# talking = False - можно вообще дальше не проверять, сразу возвращать ответ False (то есть проблем у нас нет)
# talking = True /\ hour < 7 => True (у нас проблемы, говорит ночью)
# talking = True /\ 7 <= hour <= 20 => False (в течение дня говорить можно, даже если флажок talking запрещает говорить по ночам
# talking = True /\ hour > 20 => True (опять проблемы , говорит ночью)

def parrot_trouble(talking, hour):
    if talking == False:
        return False

    else:
        #if hour < 7 and hour > 20:
        if 7 <= hour <= 20:
            return False

        else:
            return True


def parrot_trouble_2(talking, hour):
    if (talking == True) and (7 <= hour <= 20):
        return True
    return False


print("talking=False, hour=6, Ответ:", parrot_trouble(talking=False, hour=6))
print(parrot_trouble(talking=False, hour=7))
print(parrot_trouble(talking=True, hour=7))
print(parrot_trouble(talking=True, hour=20))
print(parrot_trouble(talking=True, hour=10))
print(parrot_trouble(talking=True, hour=0))
print(parrot_trouble(talking=True, hour=23))

print("talking=False, hour=6, Ответ:", parrot_trouble_2(talking=False, hour=6))
print(parrot_trouble_2(talking=False, hour=7))
print(parrot_trouble_2(talking=True, hour=7))
print(parrot_trouble_2(talking=True, hour=20))
print(parrot_trouble_2(talking=True, hour=10))
print(parrot_trouble_2(talking=True, hour=0))
print(parrot_trouble_2(talking=True, hour=23))
