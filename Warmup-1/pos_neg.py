# https://codingbat.com/prob/p162058

def pos_neg(a, b, negative):
    if negative == False:
        # if (a > 0 and b < 0) or (a < 0 and b > 0):
        # if b < 0 < a or a < 0 < b:
        A = b < 0 < a  # bla bla bla
        B = a < 0 < b

        if A or B:
            return True

        return False
        
    else:    
        if a < 0 and b < 0:
            return True

        return False
        
        
print(pos_neg(1, -1, False))
print(pos_neg(-1, 1, False))
print(pos_neg(-4, -5, True))
print(pos_neg(-1, -1, False))
print(pos_neg(1, 1, False))
print(pos_neg(1, -1, True))
print(pos_neg(1, 1, True))