# https://codingbat.com/prob/p124676

def near_hundred(n):
    if abs(n - 100) <= 10 or abs(n - 200) <= 10:
        return True

    return False


print(near_hundred(93))
print(near_hundred(90))
print(near_hundred(89))
print(near_hundred(100))
print(near_hundred(200))
print(near_hundred(193))
print(near_hundred(190))
print(near_hundred(-10))
print(near_hundred(210))
print(near_hundred(211))