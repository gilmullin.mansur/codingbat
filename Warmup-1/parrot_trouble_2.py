def parrot_trouble_2(talking, hour):
    #if (talking == True) and ((7 > hour) or (hour > 20)):
    if talking == True and (7 > hour or hour > 20):
        return True
    return False
# A |\ (B \| C)

def parrot_trouble_3(talking, hour):
    return True if talking == True and (7 > hour or hour > 20) else False

print("talking=False, hour=6, Ответ:", parrot_trouble_2(talking=False, hour=6))
print(parrot_trouble_2(talking=False, hour=7))
print(parrot_trouble_2(talking=True, hour=7))
print(parrot_trouble_2(talking=True, hour=20))
print(parrot_trouble_2(talking=True, hour=10))
print(parrot_trouble_2(talking=True, hour=0))
print(parrot_trouble_2(talking=True, hour=23))

