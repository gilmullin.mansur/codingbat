# https://codingbat.com/prob/p120546  

    
def monkey_trouble1(a_smile, b_smile):
    if a_smile == True:
        if b_smile == True:
            return True
        else:
            return False
    else:
        if b_smile == True:
            return False
        else:
            return True
            
def monkey_trouble2(a_smile, b_smile):
    return not a_smile ^ b_smile
    
print(monkey_trouble2(True, False))
print(monkey_trouble2(True, True))
print(monkey_trouble2(False, False))
print(monkey_trouble2(False, True))
