# https://codingbat.com/prob/p173401

# 1st:
#def sleep_in(weekday, vacation):
#    if weekday == True:
#        if vacation == True:
#            return True
#        else:
#            return False
#    else:
#        if vacation == True:
#            return True
#        else:
#            return True

# 2nd:
#def sleep_in(weekday, vacation):
#    result = True
    
#    if weekday == True and vacation == False:
#        result = False

#    return result
    
# 3rd:
def sleep_in(weekday, vacation):
    return False if weekday == True and vacation == False else True
    
x = sleep_in(weekday=False, vacation=False)
print(x)