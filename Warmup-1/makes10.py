# https://codingbat.com/prob/p124984

def makes10(a, b):
    if a == 10 or b == 10 or a + b == 10:
        return True

    return False
#    return True if a == 10 or b == 10 or a + b == 10 else False
    


print(makes10(9, 10))
print(makes10(9, 9))
print(makes10(1, 9))
print(makes10(0, 10))
print(makes10(-2, 12))
print(makes10(3, 5))
print(makes10(3, 15))