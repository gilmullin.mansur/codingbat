# https://codingbat.com/prob/p147920


def front3(str):
    if len(str) < 3:
        return str + str + str
    else: 
        f3_str = str[:3]
        return f3_str + f3_str + f3_str


print(front3('Java'))
print(front3('Chocolate'))
print(front3('abcpy'))




