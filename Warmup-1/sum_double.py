# https://codingbat.com/prob/p141905

# Как надо:  
def sum_double1(a, b):
    result = 0

    if isinstance(a, int) and isinstance(b, int):
        if a == b:
            result = 4 * a

        else:
            result = a + b

    else:
        print("WARNING! ONLY INT NUMBERS!")

    return result

# Под условие задачи:
def sum_double2(a, b):
    result = 0

    if isinstance(a, int) and isinstance(b, int):
        if a == b:
            result = 4 * a

        else:
            result = a + b

    return result
    
def sum_double3(a, b):
    result = a + b

    if a == b:
        result = 4 * a

        return result
    
print(sum_double3(1, 2))
print(sum_double3(3, 3))
print(sum_double3(3, 4))