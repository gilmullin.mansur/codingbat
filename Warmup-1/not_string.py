# https://codingbat.com/prob/p189441

def not_string(str):
    if str.startswith("not ") or str.lower() == "not": 
        return str
    
    return "not " + str
    
    
    