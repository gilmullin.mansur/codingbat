# https://codingbat.com/prob/p197466

def diff21(n):
    result = 0

    if isinstance(n, int):
        if n > 21:
            result = 2 * abs(21 - n)

        else:
            result = abs(21 - n)

    else:
        print("WARNING! ONLY INT NUMBERS!")

    return result


def diff21_test_1(n):
    result = abs(21 - n)

    if n > 21:
        result = 2 * result

    return result


print(diff21(19))
print(diff21(10))
print(diff21(21))
print(diff21(22))
print(diff21("111"))


print(diff21_test_1(22))

    
# Compile problems:
# unindent does not match any outer indentation level (line 4)
#see Python Example Code to help with compile problems