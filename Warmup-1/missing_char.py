# https://codingbat.com/prob/p149524

def missing_char(str, n):
    mchar = str.replace(str[n], "")
    return mchar


print(missing_char("kitten", 1))
print(missing_char("kitten", 0))
print(missing_char("kitten", 4))