# https://codingbat.com/prob/p195669

def cigar_party(cigars, is_weekend):
    result = False

    if 40 <= cigars <= 60 and is_weekend == False:
        result = True

    if 40 <= cigars and is_weekend:
        result = True

    return result

def cigar_party(cigars, is_weekend):
    result = False

    if (40 <= cigars <= 60 and is_weekend == False) or (40 <= cigars and is_weekend):
        result = True

    return result