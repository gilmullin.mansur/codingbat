# https://codingbat.com/prob/p164876


def cat_dog(str):
    ccat = 0
    cdog = 0
    for i in range(len(str) - 2):
        ccat += str[i:i + 3] == "cat"
        cdog += str[i:i + 3] == "dog"
    return True if ccat == cdog else False

# return True if str.count("cat") == str.count("dog") else False


print(cat_dog('catdog'))
print(cat_dog('catcat'))