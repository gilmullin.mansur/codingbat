# https://codingbat.com/prob/p170842


def double_char(str):
    
    temp = ""
    for char in str:
        temp += char * 2 
    return temp
    
# return "".join([char * 2 for char in str])
    
print(double_char('The'))