# https://codingbat.com/prob/p167246


def count_hi(str):
    count = 0

    for i in range(len(str) - 1):
        count += str[i:i + 2] == "hi"

    return count

# return str.count("hi")

print(count_hi('abc hi ho'))
    