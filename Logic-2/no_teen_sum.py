# https://codingbat.com/prob/p100347


def fix_teen(n):
    return 0 if n in {13, 14, 17, 18, 19} else n


def no_teen_sum(a, b, c):
    result = 0
    
    for num in (a, b, c):
        result += fix_teen(num)

    return result

