# https://codingbat.com/prob/p179960


def round10(num):
    return 10 * ((num + 5) // 10)


def round_sum(a, b, c):
    return round10(a) + round10(b) + round10(c)
