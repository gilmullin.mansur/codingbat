# https://codingbat.com/prob/p160533


def close_far(a, b, c):
    if (abs(a - b) <= 1 and abs(a - c) >= 2 and abs(b - c) >= 2) or (abs(a - c) <= 1 and abs(a - b) >= 2 and abs(b - c) >= 2):
        return True
    elif abs(a - b) >= 2 and abs(a - c) >= 2:
        return False
    else:
        return False

    deltaAB = abs(a - b)
    deltaBC = abs(b - c)
    deltaAC = abs(a - c)

    x = deltaAB <= 1 and deltaAC >= 2 and deltaBC >= 2
    y = deltaAC <= 1 and deltaAB >= 2 and deltaBC >= 2
    z = deltaAB >= 2 and deltaAC >= 2
    
    if x or y:
        return True
        
    elif z:
        return False
        
    else:
        return False


print(close_far(1, 2, 10))
print(close_far(1, 2, 3))
print(close_far(4, 1, 3))