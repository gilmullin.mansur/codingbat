# https://codingbat.com/prob/p190859


def make_chocolate(small, big, goal):
    if goal >= 5 * big:
        rem = goal - 5 * big

    else:
        rem = goal % 5
        
    if rem <= small:
        return rem
        
    return -1
    
    
    rem = goal - 5 * big if goal >= 5 * big else goal % 5
    return rem if rem <= small else -1