# https://codingbat.com/prob/p184853


def big_diff(nums):
    nums.sort(reverse=True)
    return nums[0] - nums[-1]


print(big_diff([10, 3, 5, 6]))  # 7