# https://codingbat.com/prob/p189616


def count_evens(nums):
    count = 0
    
    for n in nums:
        if n % 2 == 0:
            count += 1 
            
    return count

    #return sum([1 for n in nums if n % 2 == 0])
    #return len(list(filter(lambda x: x % 2 == 0, nums)))


print(count_evens([2, 1, 2, 3, 4]))  # 3
print(count_evens([2, 2, 0]))  # 3
print(count_evens([1, 3, 5]))  # 0