# https://codingbat.com/prob/p108886


def sum67(nums):
    canAdd = False
    sum = 0

    for n in nums:
        if n == 6:
            canAdd = True
            continue
            
        if n == 7 and canAdd == True:
            canAdd = False
            continue
            
        if canAdd == False:
            sum += n

    return sum


def sum67(nums):
    canAdd = False
    sum = 0

    for n in nums:
        if n == 6:
            canAdd = True
            continue
            
        if n == 7 and canAdd:
            canAdd = False
            continue
            
        if not canAdd:
            sum += n

    return sum

print(sum67([1, 2, 2]))  # 5
print(sum67([1, 2, 2, 6, 99, 99, 7]))  # 5
print(sum67([1, 1, 6, 7, 2]))  # 4