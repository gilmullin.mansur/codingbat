# https://codingbat.com/prob/p167025


def sum13(nums):
    i = 0
    
    while i < len(nums):
        if nums[i] == 13:
            del nums[i: i + 2]
            continue
            
        i += 1
        
    return sum(nums)



print(sum13([1, 2, 2, 1]))  # 6
print(sum13([1, 1]))  # 2
print(sum13([1, 2, 2, 1, 13]))  # 6