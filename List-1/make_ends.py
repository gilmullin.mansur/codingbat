# https://codingbat.com/prob/p124806


def make_ends(nums):
    a = nums[0]  # left bracket
    b = nums[-1]  # right bracket
    result = [a, b]
    print("result =", result)

    return result


print(make_ends([1, 2, 3]))
print(make_ends([1, 2, 3, 4]))
print(make_ends([7, 4, 6, 2]))
print(make_ends([1]))
print(make_ends([1, 2]))
    