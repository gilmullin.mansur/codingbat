# https://codingbat.com/prob/p135290


def max_end3(nums):
    if nums[0] >= nums[2]:
        return [nums[0], nums[0], nums[0]]
    return [nums[2], nums[2], nums[2]]



print(max_end3([1, 2, 3]))
print(max_end3([11, 5, 9]))
print(max_end3([2, 11, 3]))
print(max_end3([1, 1, 1]))
print(max_end3([4, 2, 4]))