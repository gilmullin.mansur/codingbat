# https://codingbat.com/prob/p137202


def caught_speeding(speed, is_birthday):
    if not is_birthday:
        return 0 if speed <= 60 else 1 if 61 <= speed <= 80 else 2
           
    else:
        return 0 if speed <= 65 else 1 if 66 <= speed <= 85 else 2
            

print(caught_speeding(60, False))  # 0
print(caught_speeding(65, False))  # 1
print(caught_speeding(65, True))  # 0
print(caught_speeding(80, False))  # 1
print(caught_speeding(50, False))  # 0
print(caught_speeding(90, True))  # 2