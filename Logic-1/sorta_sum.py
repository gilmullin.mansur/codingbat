# https://codingbat.com/prob/p116620


def sorta_sum(a, b):
    if 10 <= a + b <= 19:
        return 20

    return a + b

# return 20 if 10 <= a + b <= 19 else a + b

print(sorta_sum(3, 4)) # 7
print(sorta_sum(9, 4)) # 20
print(sorta_sum(10, 11)) # 21
print(sorta_sum(0, 10)) # 20
print(sorta_sum(9, 9)) # 20