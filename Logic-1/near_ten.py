# https://codingbat.com/prob/p165321


def near_ten(num):
    return True if (num % 10) <= 2 or 8 <= (num % 10) <= 9 else False


print(near_ten(12))  # True
print(near_ten(19))  # True
print(near_ten(17))  # False
print(near_ten(3))  # False
print(near_ten(25))  # False
print(near_ten(22))  # True
print(near_ten(20))  # True
