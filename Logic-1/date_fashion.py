# https://codingbat.com/prob/p129125


def date_fashion(you, date):
    """
    docstring - описание функции, которые видно в IDE
    Кратко описывают, обычно по английски, что делает 
    эта функция, с примерами
    """
    if you <= 2 or date <= 2:
        return 0
        
    elif (you >= 8 and date > 2) or (you > 2 and date >= 8):
        return 2

    else:
        return 1
            

print(date_fashion(5, 10))  # 2
print(date_fashion(5, 2))  # 0
print(date_fashion(5, 5))  # 1
print(date_fashion(2, 9))  # 0
print(date_fashion(1, 4))  # 0