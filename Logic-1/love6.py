# https://codingbat.com/prob/p100958


def love6(a, b):
    if a == 6 or b == 6:
        return True

    elif a + b == 6 or abs(a - b) == 6:
        return True

    else:
        return False

# return True if a == 6 or b == 6 or a + b == 6 or abs(a - b) == 6 else False


print(love6(6, 4))
print(love6(3, 6))
print(love6(4, 5))
print(love6(1, 5))
print(love6(7, 1))
print(love6(2, 7))
print(love6(-7, 1)) # False ?
print(love6(-2, -4)) # False ?