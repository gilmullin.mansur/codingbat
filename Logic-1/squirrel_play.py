# https://codingbat.com/prob/p135815


def squirrel_play(temp, is_summer):
    if not is_summer:
        if 60 <= temp <= 90:
            return True

        else:
            return False

    else:
        if 60 <= temp <= 100:
            return True

        else:
            return False

# if not is_summer:
#     return True if 60 <= temp <= 90 else False
# 
# else:
#     return True if 60 <= temp <= 100 else False


print(squirrel_play(70, False))  # True
print(squirrel_play(95, False))  # False
print(squirrel_play(95, True))  # True
print(squirrel_play(50, False))  # False
print(squirrel_play(50, True))  # False
print(squirrel_play(100, True))  # True
print(squirrel_play(60, True))  # True
print(squirrel_play(60, False))  # True