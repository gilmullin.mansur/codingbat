# https://codingbat.com/prob/p158497


def in1to10(n, outside_mode):
    if outside_mode:
        return True if n <= 1 or n >= 10 else False
    
    else:
        return True if 1 <= n <= 10 else False
        

print(in1to10(5, False))  # True
print(in1to10(11, False))  # False
print(in1to10(11, True))  # True
print(in1to10(1, False))  # True
print(in1to10(5, True))  # False
print(in1to10(10, True))  # True