# https://codingbat.com/prob/p194053


def combo_string(a, b):
    if len(a) < len(b):
        return a + b + a

    return b + a + b


print(combo_string('Hello', 'hi'))
print(combo_string('hi', 'Hello'))
print(combo_string('aaa', 'b'))
print(combo_string('a', ''))
print(combo_string('', ''))
