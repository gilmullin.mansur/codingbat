# https://codingbat.com/prob/p127703


def non_start(a, b):
    return a[1:] + b[1:]


print(non_start('Hello', 'There'))
print(non_start('java', 'code'))
print(non_start('shotl', 'java'))
print(non_start('a', 'bb'))
print(non_start('a', 'b'))