# https://codingbat.com/prob/p132290


# constants:
OPEN_TAG_LEFT = '<'
OPEN_TAG_RIGHT = '>'
CLOSE_TAG_LEFT = '</'
CLOSE_TAG_RIGHT = '>'


def make_tags(tag, word):
    return OPEN_TAG_LEFT + tag + OPEN_TAG_RIGHT + word + CLOSE_TAG_LEFT + tag + CLOSE_TAG_RIGHT


print(make_tags('i', 'Yay'))
print(make_tags('i', 'Hello'))
print(make_tags('cite', 'Yay'))