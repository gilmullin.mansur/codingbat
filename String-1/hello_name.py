# https://codingbat.com/prob/p115413


def hello_name(name):
    return "Hello " + name + "!"
    
    
print(hello_name('Bob'))
print(hello_name('Alice'))
print(hello_name('X'))