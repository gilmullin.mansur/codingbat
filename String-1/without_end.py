# https://codingbat.com/prob/p138533


def without_end(str):
    return str[1:-1]


print(without_end('Hello'))
print(without_end('java'))
print(without_end('coding'))
print(without_end('ab'))
print(without_end('a'))
print(without_end(''))