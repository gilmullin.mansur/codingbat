# https://codingbat.com/prob/p129981
def make_out_word(out, word):
    return out[:2] + word + out[2:]
    # return out[0] + out[1] + word + out[2] + out[3]


print(make_out_word('<<>>', 'Yay'))
print(make_out_word('<<>>', 'WooHoo'))
print(make_out_word('[[]]', 'word'))