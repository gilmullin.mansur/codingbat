# https://codingbat.com/prob/p148853
def extra_end(str):
    if len(str) > 1:
        return str[-2:] + str[-2:] + str[-2:]
        # return str[-2:] * 3
    return ""
    # return str[-2:] * 3 if len(str) > 1 else ""
    return str[-2:] * 3

print(extra_end('Hello')) 
print(extra_end('ab'))
print(extra_end('Hi'))
print(extra_end('X'))   