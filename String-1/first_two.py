# https://codingbat.com/prob/p184816


def first_two(str):
    if len(str) > 1:
        return str[:2]

    return str


print(first_two('Hello'))
print(first_two('abcdefg'))
print(first_two('ab'))
print(first_two('X'))
print(first_two(''))