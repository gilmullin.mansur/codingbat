# https://codingbat.com/prob/p107010

def first_half(str):
    if len(str) % 2 == 0:
        #n = int(len(str)/2)
        #print(n)
        #return str[:n]
        #return str[:int(len(str)/2)]
        return str[:len(str)//2]

    return ""


print(first_half('WooHoo'))
print(first_half('HelloThere'))
print(first_half('abcdef'))
print(first_half('Ho'))
print(first_half('W'))
print(first_half('Woo'))